# arc AUR Helper
arc is a AUR Helper for Arch Linux.

## Compiling
1. Clone the repository using git: `git clone https://github.com/fgclue/arc && cd arc`
2. Compile arc `make`
